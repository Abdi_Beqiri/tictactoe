﻿

using System.Collections.Generic;
using TicTacToeGame;

namespace TicTacToeTest
{
    public static class TestUtils
    {
        public static void PlayGame(Board board, bool isDraw = false)
        {
            var isX = true;
            foreach (var move in GetMoves(isDraw))
            {
                board[move] = isX ? "X" : "O";
                if (isX)
                    board[move] = "X";
                else
                    board[move] = "O";
                isX = !isX;
            }
        }

        public static void PlayGame(TicTacToe ticTacToe, bool isDraw = false)
        {            
            foreach (var move in GetMoves(isDraw))
                ticTacToe.PlayMove(move);
        }

        private static List<int> GetMoves(bool isDraw)
        {
            var drawMoves = new List<int> { 0, 1, 2, 8, 7, 6, 3, 4, 5 };
            var winningMoves = new List<int> { 0, 8, 1, 7, 2, 6, 3, 5, 4 };
            return isDraw ? drawMoves : winningMoves;
        }
    }
}
