﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TicTacToeGame;

namespace TicTacToeTest
{
    /// <summary>
    /// Summary description for GameEngineTest
    /// </summary>
    [TestClass]
    public class GameEngineTest
    {
        [TestMethod]
        public void TestInitialization()
        {
        }

        [TestMethod]
        public void CheckCenterMoveTest()
        {
            //Arrange
            var board = new Board();
            var gameEngine = new GameEngine(board);
            board[0] = "X";
            board[1] = "O";
            //Act
            var move = gameEngine.SimpleStrategy("X");

            //Assert
            Assert.AreEqual(board[move] = "X", board[4]);
        }

        [TestMethod]
        public void CheckCornerMoveTest()
        {
            //Arrange
            var board = new Board();
            var gameEngine = new GameEngine(board);
            board[4] = "X";
            board[1] = "O";
            //Act
            var move = gameEngine.SimpleStrategy("X");

            //Assert
            Assert.AreEqual(board[move] = "X", board[0]);
        }

        [TestMethod]
        public void CheckWinningMoveTest()
        {
            //Arrange
            var board = new Board();
            var gameEngine = new GameEngine(board);
            board[0] = "X";
            board[1] = "O";
            board[3] = "X";
            board[4] = "O";
            //Act
            var move = gameEngine.SimpleStrategy("X");

            //Assert
            Assert.AreEqual(board[move] = "X", board[6]);
        }

        [TestMethod]
        public void CheckBlockMoveTest()
        {
            //Arrange
            var board = new Board();
            var gameEngine = new GameEngine(board);
            board[0] = "X";
            board[1] = "O";
            board[8] = "X";
            board[4] = "O";
            //Act
            var move = gameEngine.SimpleStrategy("X");

            //Assert
            Assert.AreEqual(board[move] = "X", board[7]);
        }
    }
}
