﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TicTacToeGame;

namespace TicTacToeTest
{
    [TestClass]
    public class TicTacToeTest
    {
        bool _isGameOver;
        int[] _winnerCombination;

        [TestMethod]
        public void TestInitialization()
        {
        }

        [TestMethod]
        public void ResetGameTest()
        {
            //Arrange
            var board = new Board();
            var ticTacToe = new TicTacToe(board);
            ticTacToe.PlayMove(1);

            //Act
            ticTacToe.ResetGame();

            //Assert
            Assert.IsTrue(ticTacToe.IsXTurn);
            Assert.IsNull(board[0]);
        }

        [TestMethod]
        public void PlayMoveTest()
        {
            //Arrange
            var board = new Board();
            var ticTacToe = new TicTacToe(board);

            //Act
            ticTacToe.PlayMove(0);
            ticTacToe.PlayMove(1);

            //Assert
            Assert.AreEqual("X", board[0]);
            Assert.AreEqual("O", board[1]);
        }

        [TestMethod]
        public void GameOverWinningTest()
        {
            //Arrange
            var board = new Board();
            var ticTacToe = new TicTacToe(board);
            ticTacToe.MovePlayed += new System.EventHandler<MovePlayedEventArgs>(GameOverHandler);
            _isGameOver = false;
            _winnerCombination = null;

            //Act
            TestUtils.PlayGame(ticTacToe);

            //Assert
            Assert.IsTrue(_isGameOver);
            Assert.AreEqual(0, _winnerCombination[0]);
            Assert.AreEqual(1, _winnerCombination[1]);
            Assert.AreEqual(2, _winnerCombination[2]);
        }


        [TestMethod]
        public void GameOverDrawTest()
        {
            //Arrange
            var board = new Board();
            var ticTacToe = new TicTacToe(board);
            ticTacToe.MovePlayed += new System.EventHandler<MovePlayedEventArgs>(GameOverHandler);
            _isGameOver = false;
            _winnerCombination = null;

            //Act
            TestUtils.PlayGame(ticTacToe, true);

            //Assert
            Assert.IsTrue(_isGameOver);
            Assert.IsNull(_winnerCombination);
        }

        private void GameOverHandler(object sender, MovePlayedEventArgs e)
        {
            _isGameOver = true;
            _winnerCombination = e.WinnerCombination;
        }
    }
}
