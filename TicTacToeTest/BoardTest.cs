﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TicTacToeGame;

namespace TicTacToeTest
{
    [TestClass]
    public class BoardTest
    {
        [TestMethod]
        public void TestInitialization()
        {
        }

        [TestMethod]
        public void ResetGameTest()
        {
            //Arrange
            var board = new Board();
            //Act

            board[0] = "X";
            board.ResetBoard();

            //Assert
            Assert.IsNull(board[0]);

        }

        [TestMethod]
        public void SetFieldValueTest()
        {
            //Arrange
            var board = new Board();
            //Act

            board[0] = "X";

            //Assert
            Assert.AreEqual("X", board[0]);

        }

        [TestMethod]
        public void GetFieldValueTest()
        {
            //Arrange
            var board = new Board();
            //Act

            board[0] = "X";
            board[1] = "O";

            //Assert
            Assert.IsNull(board[8]);
            Assert.AreEqual("X", board[0]);
            Assert.AreEqual("O", board[1]);

        }

        [TestMethod]
        public void IsGameOverTest()
        {
            //Arrange
            var board = new Board();
            
            //Act
            TestUtils.PlayGame(board);

            //Assert
            Assert.IsTrue(board.IsGameOver());

        }

        [TestMethod]
        public void WinnerCombinationTest()
        {
            //Arrange
            var board = new Board();
            
            //Act
            TestUtils.PlayGame(board);
            var winner = board.GetWinnerCombination();

            //Assert
            Assert.AreEqual(0, winner[0]);
            Assert.AreEqual(1, winner[1]);
            Assert.AreEqual(2, winner[2]);
        }

        [TestMethod]
        public void DrawCombinationTest()
        {
            //Arrange
            var board = new Board();
            //Act

            TestUtils.PlayGame(board, true);
            var winner = board.GetWinnerCombination();

            //Assert
            Assert.IsNull(winner);
        }


    }
}
