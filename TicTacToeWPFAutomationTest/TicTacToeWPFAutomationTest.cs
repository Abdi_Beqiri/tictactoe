﻿using TestStack.White;
using TestStack.White.UIItems;
using TestStack.White.Factory;
using TestStack.White.UIItems.Finders;
using TestStack.White.InputDevices;
using Castle.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;


namespace TicTacToeWPFAutomationTest
{
    [TestClass]
    public class TicTacToeWPFAutomationTest
    {
        private const string ExeSourceFile = @"C:\Users\X\source\repos\TicTacToe\TicTacToeWPF\bin\Debug\TicTacToeWPF.exe";
        private static TestStack.White.Application _application;
        private static TestStack.White.UIItems.WindowItems.Window _mainWindow;

        private void StartUpApplication()
        {
            var psi = new ProcessStartInfo(ExeSourceFile);
            _application = Application.AttachOrLaunch(psi);
            _mainWindow = _application.GetWindow(SearchCriteria.ByText("TicTacToe"), InitializeOption.NoCache);
        }
        [TestMethod]
        public void Game_Can_Start()
        {

            //Arrange            
            StartUpApplication();
            //Act
            
            Label labelTurn = _mainWindow.Get<Label>(SearchCriteria.ByAutomationId("labelTurn"));

            //Assert
            Assert.AreEqual("X's turn", labelTurn.Text);
            
            _application.Kill();
        }

        [TestMethod]
        public void Can_Reset_Gamet()
        {

            //Arrange            
            StartUpApplication();
            //Act
            Label labelTurn = _mainWindow.Get<Label>(SearchCriteria.ByAutomationId("labelTurn"));
            Button button1 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("but1"));
            button1.Click();
            Button playButton = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("PlayButton"));
            playButton.Click();

            //Assert
            Assert.AreEqual("", button1.Text);
            Assert.AreEqual("X's turn", labelTurn.Text);
            
            _application.Kill();
        }

        [TestMethod]
        public void Player_X_Can_Pick()
        {

            //Arrange            
            StartUpApplication();
            //Act
            Button button1 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("but1"));
            button1.Click();
            Label labelTurn = _mainWindow.Get<Label>(SearchCriteria.ByAutomationId("labelTurn"));

            //Assert
            Assert.AreEqual("X", button1.Text);
            Assert.AreEqual("O's turn", labelTurn.Text);
            
            _application.Kill();
        }

        [TestMethod]
        public void Player_O_Can_Pick()
        {

            //Arrange            
            StartUpApplication();

            Button button1 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("but1"));
            button1.Click();
            Label labelTurn = _mainWindow.Get<Label>(SearchCriteria.ByAutomationId("labelTurn"));
            //Act
            Button button2 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("but2"));
            button2.Click();
            //Assert
            Assert.AreEqual("O", button2.Text);
            Assert.AreEqual("X's turn", labelTurn.Text);
            
            _application.Kill();
        }

        [TestMethod]
        public void Player_X_Can_Win()
        {
            //Arrange            
            StartUpApplication();

            Label labelTurn = _mainWindow.Get<Label>(SearchCriteria.ByAutomationId("labelTurn"));
            Button button1 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("but1"));
            Button button2 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("but2"));
            Button button3 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("but3"));

            Button button9 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("but9"));
            Button button8 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("but8"));

            button1.Click();
            button9.Click();
            button2.Click();
            button8.Click();
            //Act
            button3.Click();
            //Assert
            Assert.AreEqual("X", button1.Text);
            Assert.AreEqual("X", button2.Text);
            Assert.AreEqual("X", button3.Text);
            Assert.AreEqual("O", button9.Text);
            Assert.AreEqual("O", button8.Text);
            Assert.AreEqual("Game over!", labelTurn.Text);
            
            _application.Kill();
        }

        [TestMethod]
        public void Player_O_Can_Win()
        {
            //Arrange            
            StartUpApplication();

            Label labelTurn = _mainWindow.Get<Label>(SearchCriteria.ByAutomationId("labelTurn"));
            Button button1 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("but1"));
            Button button2 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("but2"));
            Button button3 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("but3"));

            Button button9 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("but9"));
            Button button8 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("but8"));
            Button button5 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("but5"));

            button9.Click();
            button1.Click();
            button8.Click();
            button2.Click();
            button5.Click();
            //Act
            button3.Click();
            //Assert
            Assert.AreEqual("X", button9.Text);
            Assert.AreEqual("X", button8.Text);
            Assert.AreEqual("X", button5.Text);
            Assert.AreEqual("O", button1.Text);
            Assert.AreEqual("O", button2.Text);
            Assert.AreEqual("O", button3.Text);
            Assert.AreEqual("Game over!", labelTurn.Text);
            
            _application.Kill();
        }

        [TestMethod]
        public void Play_Versus_Computer()
        {

            //Arrange            
            StartUpApplication();
            Label labelTurn = _mainWindow.Get<Label>(SearchCriteria.ByAutomationId("labelTurn"));
            Button button1 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("but1"));
            Button button5 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("but5"));


            //Act
            Button playButton = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("PlayButton"));
            RadioButton versusComputer = _mainWindow.Get<RadioButton>(SearchCriteria.ByAutomationId("VersusComputerButton"));
            versusComputer.Click();
            playButton.Click();
            button1.Click();

            //Assert
            Assert.AreEqual("O", button5.Text);
            Assert.AreEqual("X", button1.Text);
            Assert.AreEqual("X's turn", labelTurn.Text);
            
            _application.Kill();
        }

        [TestMethod]
        public void Player_Can_Win_Versus_Computer()
        {
            //Arrange            
            StartUpApplication();

            //Playing againts computer
            Button playButton = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("PlayButton"));
            RadioButton versusComputer = _mainWindow.Get<RadioButton>(SearchCriteria.ByAutomationId("VersusComputerButton"));
            _mainWindow.WaitWhileBusy();
            versusComputer.Click();
            playButton.Click();

            //Button declaration
            Label labelTurn = _mainWindow.Get<Label>(SearchCriteria.ByAutomationId("labelTurn"));
            Button button1 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("but1"));
            Button button3 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("but3"));
            Button button4 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("but4"));
            Button button5 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("but5"));
            Button button7 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("but7"));
            Button button8 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("but8"));
            Button button9 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("but9"));




            //Act
            button1.Click();
            button9.Click();
            button7.Click();
            button8.Click();

            //Assert
            Assert.AreEqual("X", button1.Text);
            Assert.AreEqual("X", button9.Text);
            Assert.AreEqual("X", button7.Text);
            Assert.AreEqual("X", button8.Text);
            Assert.AreEqual("O", button5.Text);
            Assert.AreEqual("O", button3.Text);
            Assert.AreEqual("O", button4.Text);
            Assert.AreEqual("Game over!", labelTurn.Text);
            
            _application.Kill();
        }

        [TestMethod]
        public void Computer_Wins()
        {
            //Arrange            
            StartUpApplication();

            //Playing againts computer
            Button playButton = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("PlayButton"));
            RadioButton versusComputer = _mainWindow.Get<RadioButton>(SearchCriteria.ByAutomationId("VersusComputerButton"));
            _mainWindow.WaitWhileBusy();
            versusComputer.Click();
            playButton.Click();

            //Button declaration
            Label labelTurn = _mainWindow.Get<Label>(SearchCriteria.ByAutomationId("labelTurn"));
            Button button3 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("but3"));
            Button button5 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("but5"));
            Button button6 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("but6"));
            Button button7 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("but7"));
            Button button8 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("but8"));
            Button button9 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("but9"));

            //Act
            button9.Click();
            button8.Click();
            button6.Click();

            //Assert
            Assert.AreEqual("O", button5.Text);
            Assert.AreEqual("O", button7.Text);
            Assert.AreEqual("O", button3.Text);
            Assert.AreEqual("X", button9.Text);
            Assert.AreEqual("X", button8.Text);
            Assert.AreEqual("X", button6.Text);
            Assert.AreEqual("Game over!", labelTurn.Text);
            
            _application.Kill();
        }

        [TestMethod]
        public void Game_Can_Be_Draw()
        {
            //Arrange            
            StartUpApplication();

            //Playing againts computer
            Button playButton = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("PlayButton"));
            RadioButton versusComputer = _mainWindow.Get<RadioButton>(SearchCriteria.ByAutomationId("VersusComputerButton"));
            _mainWindow.WaitWhileBusy();
            versusComputer.Click();
            playButton.Click();

            //Button declaration
            Label labelTurn = _mainWindow.Get<Label>(SearchCriteria.ByAutomationId("labelTurn"));
            Button button1 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("but1"));
            Button button2 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("but2"));
            Button button3 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("but3"));
            Button button4 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("but4"));
            Button button5 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("but5"));
            Button button6 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("but6"));
            Button button7 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("but7"));
            Button button8 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("but8"));
            Button button9 = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("but9"));




            //Act
            button1.Click();
            button3.Click();
            button8.Click();
            button4.Click();
            button6.Click();

            //Assert
            Assert.AreEqual("X", button1.Text);
            Assert.AreEqual("X", button3.Text);
            Assert.AreEqual("X", button8.Text);
            Assert.AreEqual("X", button4.Text);
            Assert.AreEqual("X", button6.Text);
            Assert.AreEqual("O", button2.Text);
            Assert.AreEqual("O", button5.Text);
            Assert.AreEqual("O", button7.Text);
            Assert.AreEqual("O", button9.Text);
            Assert.AreEqual("Game over!", labelTurn.Text);
            
            _application.Kill();
        }
    }
}
