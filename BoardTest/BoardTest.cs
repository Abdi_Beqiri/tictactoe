﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BoardTest
{
    [TestClass]
    public class BoardTest
    {
        [TestMethod]
        public void TestInitialization()
        {
        }

        [TestMethod]
        public void ResetGameTest()
        {
            //Arrange
            var board = new TicTacToe.Board();
            //Act

            board.SetFieldValue(0, "X");
            board.ResetBoard();


            //Assert
            Assert.IsNull(board.GetFieldValue(0));

        }

        [TestMethod]
        public void SetFieldValueTest()
        {
            //Arrange
            var board = new TicTacToe.Board();
            //Act

            board.SetFieldValue(0, "X");


            //Assert
            Assert.AreEqual("X", board.GetFieldValue(0));

        }

        [TestMethod]
        public void GetFieldValueTest()
        {
            //Arrange
            var board = new TicTacToe.Board();
            //Act

            board.SetFieldValue(0, "X");
            board.SetFieldValue(1, "O");


            //Assert
            Assert.IsNull(board.GetFieldValue(8));
            Assert.AreEqual("X", board.GetFieldValue(0));
            Assert.AreEqual("O", board.GetFieldValue(1));

        }

        [TestMethod]
        public void IsGameOverTest()
        {
            //Arrange
            var board = new TicTacToe.Board();
            //Act

            PlayGame(board);


            //Assert
            Assert.IsTrue(board.IsGameOver());

        }

        [TestMethod]
        public void WinnerCombinationTest()
        {
            //Arrange
            var board = new TicTacToe.Board();
            //Act

            PlayGame(board);
            var winner = board.GetWinnerCombination();

            //Assert
            Assert.AreEqual(0, winner[0]);
            Assert.AreEqual(1, winner[1]);
            Assert.AreEqual(2, winner[2]);
        }

        private void PlayGame(TicTacToe.Board board)
        {
            int xPosition = 0;
            int oPosition = 8;
            bool isXTurn = true;
            while (!board.IsGameOver())
            {

                if (isXTurn)    // Alternate putting the X or O character in the Text property
                {
                    board.SetFieldValue(xPosition, "X");
                    xPosition++;
                }
                else
                {
                    board.SetFieldValue(oPosition, "O");
                    oPosition--;
                }
                isXTurn = !isXTurn;
            }
        }


    }
}
