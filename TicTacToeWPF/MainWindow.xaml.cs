﻿using System;
using System.Drawing;
using System.Windows;
using System.Windows.Controls;
using TicTacToeGame;


namespace TicTacToeWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private Button[] _buttonArray = new Button[9];
        private TicTacToe _ticTacToe = new TicTacToe();
        private bool _isTwoPlayers = true;

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            _buttonArray = new Button[9] { but1, but2, but3, but4, but5, but6, but7, but8, but9 };
            for (int i = 0; i < 9; i++)
                _buttonArray[i].Click += new RoutedEventHandler(ClickHandler); 

            ResetButtons();  // set the defaults
            _ticTacToe.ResetGame();
            _ticTacToe.MovePlayed += new EventHandler<MovePlayedEventArgs>(MovePlayedHandler);
        }

        public MainWindow()
        {
            InitializeComponent();
        }

        

        private void PlayButton_Click(object sender, RoutedEventArgs e)
        {
            _isTwoPlayers = (TwoPlayersButton.IsChecked == true);
            ResetButtons();
            _ticTacToe.ResetGame();
            labelTurn.Content = "X's turn";
        }

        private void ResetButtons()
        {
            for (int i = 0; i < 9; i++)
            {
                _buttonArray[i].Tag = i.ToString();
                _buttonArray[i].Content = "";
                _buttonArray[i].Foreground = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromRgb(0,0,0));
                _buttonArray[i].Background = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromRgb(221, 221, 221));
                _buttonArray[i].FontFamily = new System.Windows.Media.FontFamily("Microsoft Sans Serif");
                _buttonArray[i].FontSize = 75F;
                _buttonArray[i].IsHitTestVisible = true;
            }
        }

        private void ClickHandler(object sender, RoutedEventArgs e) 
        {
            int buttonPosition = int.Parse(((Button)sender).Tag.ToString());
            _ticTacToe.PlayMove(buttonPosition);

            if (!_isTwoPlayers)
                _ticTacToe.PlayComputerMove();

        }

        private void MovePlayedHandler(object sender, MovePlayedEventArgs e)
        {
            // Alternate putting the X or O character in the Text property
            labelTurn.Content = _ticTacToe.IsXTurn ? "O's turn" : "X's turn";
            UpdateButton(e.MovePlayed);
            if (e.IsGameOver)
            {
                labelTurn.Content = "Game over!";
                ChangeColor(_buttonArray, e.WinnerCombination);
            }
        }

        private void UpdateButton(int i)
        {
            _buttonArray[i].Content = _ticTacToe.IsXTurn ? "X" : "O";
            _buttonArray[i].IsHitTestVisible = false;
        }

        private void ChangeColor(Button[] buttonArray, int[] winnerCombination)
        {            
            if (winnerCombination != null)
            {
                foreach (var butonId in winnerCombination)
                {
                    _buttonArray[butonId].Background = 
                        new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromRgb(200, 50, 50)); ;                    
                }
            }

            foreach (var button in _buttonArray)
            {
                button.IsHitTestVisible = false;
            }
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            var min = (float)Math.Min(ActualHeight, ActualWidth);

            //Button font resizing
            var fontSize = (min / 6);
            foreach (var button in _buttonArray)
            {
                if (button != null)
                    for (int i = 0; i < 9; i++)
                    {
                        _buttonArray[i].FontSize = fontSize;
                    }

            }

            //Label resizing
            labelTurn.Height = ActualHeight / 6;
            labelTurn.FontSize = min / 10;
        }
    }
}
