﻿using System;

namespace TicTacToeGame
{
    public class TicTacToe
    {
        private readonly Board _board;
        private readonly GameEngine _gameEngine;

        public TicTacToe(Board board = null)
        {
            IsXTurn = true;
            _board = board ?? new Board();
            _gameEngine = new GameEngine(_board);
        }

        // to determine if X or O is current character
        public bool IsXTurn { get; private set; }

        public void ResetGame()
        {
            IsXTurn = true;
            _board.ResetBoard();
        }

        public int PlayComputerMove()
        {
                var position = _gameEngine.SimpleStrategy(IsXTurn ? "X" : "O");
                if (position != -1)
                    PlayMove(position);
                return position;
        }
        public void PlayMove(int position)
        {
                var value = IsXTurn ? "X" : "O";
                _board[position] = value;               
                NotifyMovePlayed(position);
                ChangePlayers();
        }

        private void NotifyMovePlayed(int move)
        {
            var args = new MovePlayedEventArgs
            {
                MovePlayed = move,
                IsGameOver = _board.IsGameOver(),
                WinnerCombination = _board.GetWinnerCombination()
            };
            OnMovePlayed(args);
        }

        private void ChangePlayers()
        {
            IsXTurn = !IsXTurn;
        }

        public event EventHandler<MovePlayedEventArgs> MovePlayed;

        private void OnMovePlayed(MovePlayedEventArgs e)
        {
            MovePlayed?.Invoke(this, e);
        }
    }

    public class MovePlayedEventArgs: EventArgs
    {
        public int MovePlayed;
        public bool IsGameOver;
        public int[] WinnerCombination;
    }
}