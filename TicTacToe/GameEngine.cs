﻿//TODO implement game engine for the game

using System.Collections.Generic;
using System;
using System.Linq;

namespace TicTacToeGame
{
    public class GameEngine
    {
        private Board _board;
        private readonly List<int> _corners = new List<int> { 0, 2, 6, 8 };
        private readonly List<int> _sides = new List<int> { 1, 3, 5, 7 };
        private const int _center = 4;

        public GameEngine(Board board)
        {
            _board = board ?? throw new Exception("A board must be defined!");
        }

        private List<int> AllowedMoves()
        {
            var result = new List<int>(Board.BoardSize);
            for (var i = 0; i < Board.BoardSize; i++)
                if (_board[i] == null)
                    result.Add(i);
            return result;
        }
       
        public int SimpleStrategy(string value)
        {
            var allowedMoves = AllowedMoves();
            if (_board.IsGameOver() || allowedMoves.Count == 0)
                return -1;

            foreach (var move in allowedMoves)
                if (CheckForWin(move, value))
                    return move;

            var otherValue = value == "X" ? "O" : "X";

            foreach (var move in allowedMoves)
                if (CheckForWin(move, otherValue))
                    return move;

            if (allowedMoves.Contains(_center))
                return _center;

            var allowedCorners = allowedMoves.Intersect(_corners);

            if (allowedCorners.Any())
                return allowedCorners.First();

            var allowedSides = allowedMoves.Intersect(_sides);

            if (allowedSides.Any())
                return allowedSides.First();

            return -1;
        }

        private bool CheckForWin(int move, string value)
        {
            _board[move] = value;
            var winner = _board.GetWinnerCombination();
            var hasWin = winner != null && winner.Any(x => x == move);
            _board[move] = null;

            return hasWin;
        }
    }
}