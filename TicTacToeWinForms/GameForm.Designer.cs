﻿namespace TicTacToeWinForms
{
    partial class GameForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.but1 = new System.Windows.Forms.Button();
            this.but2 = new System.Windows.Forms.Button();
            this.but3 = new System.Windows.Forms.Button();
            this.but4 = new System.Windows.Forms.Button();
            this.but5 = new System.Windows.Forms.Button();
            this.but6 = new System.Windows.Forms.Button();
            this.but7 = new System.Windows.Forms.Button();
            this.but8 = new System.Windows.Forms.Button();
            this.but9 = new System.Windows.Forms.Button();
            this.PlayButton = new System.Windows.Forms.Button();
            this.labelTurn = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.VersusComputerButton = new System.Windows.Forms.RadioButton();
            this.TwoPlayersButton = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // but1
            // 
            this.but1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.but1.Location = new System.Drawing.Point(3, 3);
            this.but1.Name = "but1";
            this.but1.Size = new System.Drawing.Size(58, 58);
            this.but1.TabIndex = 0;
            this.but1.UseVisualStyleBackColor = true;
            // 
            // but2
            // 
            this.but2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.but2.Location = new System.Drawing.Point(67, 3);
            this.but2.Name = "but2";
            this.but2.Size = new System.Drawing.Size(58, 58);
            this.but2.TabIndex = 1;
            this.but2.UseVisualStyleBackColor = true;
            // 
            // but3
            // 
            this.but3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.but3.Location = new System.Drawing.Point(131, 3);
            this.but3.Name = "but3";
            this.but3.Size = new System.Drawing.Size(59, 58);
            this.but3.TabIndex = 2;
            this.but3.UseVisualStyleBackColor = true;
            // 
            // but4
            // 
            this.but4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.but4.Location = new System.Drawing.Point(3, 67);
            this.but4.Name = "but4";
            this.but4.Size = new System.Drawing.Size(58, 58);
            this.but4.TabIndex = 3;
            this.but4.UseVisualStyleBackColor = true;
            // 
            // but5
            // 
            this.but5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.but5.Location = new System.Drawing.Point(67, 67);
            this.but5.Name = "but5";
            this.but5.Size = new System.Drawing.Size(58, 58);
            this.but5.TabIndex = 4;
            this.but5.UseVisualStyleBackColor = true;
            // 
            // but6
            // 
            this.but6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.but6.Location = new System.Drawing.Point(131, 67);
            this.but6.Name = "but6";
            this.but6.Size = new System.Drawing.Size(59, 58);
            this.but6.TabIndex = 5;
            this.but6.UseVisualStyleBackColor = true;
            // 
            // but7
            // 
            this.but7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.but7.Location = new System.Drawing.Point(3, 131);
            this.but7.Name = "but7";
            this.but7.Size = new System.Drawing.Size(58, 59);
            this.but7.TabIndex = 6;
            this.but7.UseVisualStyleBackColor = true;
            // 
            // but8
            // 
            this.but8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.but8.Location = new System.Drawing.Point(67, 131);
            this.but8.Name = "but8";
            this.but8.Size = new System.Drawing.Size(58, 59);
            this.but8.TabIndex = 7;
            this.but8.UseVisualStyleBackColor = true;
            // 
            // but9
            // 
            this.but9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.but9.Location = new System.Drawing.Point(131, 131);
            this.but9.Name = "but9";
            this.but9.Size = new System.Drawing.Size(59, 59);
            this.but9.TabIndex = 8;
            this.but9.UseVisualStyleBackColor = true;
            // 
            // PlayButton
            // 
            this.PlayButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PlayButton.Location = new System.Drawing.Point(166, 328);
            this.PlayButton.Name = "PlayButton";
            this.PlayButton.Size = new System.Drawing.Size(128, 39);
            this.PlayButton.TabIndex = 9;
            this.PlayButton.Text = "Play";
            this.PlayButton.UseVisualStyleBackColor = true;
            this.PlayButton.Click += new System.EventHandler(this.ButtonPlay_Click);
            // 
            // labelTurn
            // 
            this.labelTurn.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelTurn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelTurn.Location = new System.Drawing.Point(0, 0);
            this.labelTurn.Name = "labelTurn";
            this.labelTurn.Size = new System.Drawing.Size(443, 38);
            this.labelTurn.TabIndex = 10;
            this.labelTurn.Text = "X\'s turn";
            this.labelTurn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Controls.Add(this.but1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.but2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.but4, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.but9, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.but5, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.but6, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.but8, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.but3, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.but7, 0, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(64, 64);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(193, 193);
            this.tableLayoutPanel1.TabIndex = 11;
            this.tableLayoutPanel1.SizeChanged += new System.EventHandler(this.TableLayoutPanel1_SizeChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.VersusComputerButton);
            this.groupBox1.Controls.Add(this.TwoPlayersButton);
            this.groupBox1.Location = new System.Drawing.Point(289, 113);
            this.groupBox1.MaximumSize = new System.Drawing.Size(150, 100);
            this.groupBox1.MinimumSize = new System.Drawing.Size(140, 100);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(142, 100);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Player selection";
            // 
            // VersusComputerButton
            // 
            this.VersusComputerButton.AutoSize = true;
            this.VersusComputerButton.Location = new System.Drawing.Point(17, 67);
            this.VersusComputerButton.Name = "VersusComputerButton";
            this.VersusComputerButton.Size = new System.Drawing.Size(110, 21);
            this.VersusComputerButton.TabIndex = 0;
            this.VersusComputerButton.Text = "Vs Computer";
            this.VersusComputerButton.UseVisualStyleBackColor = true;
            // 
            // TwoPlayersButton
            // 
            this.TwoPlayersButton.AutoSize = true;
            this.TwoPlayersButton.Checked = true;
            this.TwoPlayersButton.Location = new System.Drawing.Point(17, 31);
            this.TwoPlayersButton.Name = "TwoPlayersButton";
            this.TwoPlayersButton.Size = new System.Drawing.Size(106, 21);
            this.TwoPlayersButton.TabIndex = 0;
            this.TwoPlayersButton.TabStop = true;
            this.TwoPlayersButton.Text = "Two Players";
            this.TwoPlayersButton.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(443, 419);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.labelTurn);
            this.Controls.Add(this.PlayButton);
            this.MinimumSize = new System.Drawing.Size(400, 400);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button PlayButton;
        private System.Windows.Forms.Label labelTurn;
        internal System.Windows.Forms.Button but1;
        internal System.Windows.Forms.Button but2;
        internal System.Windows.Forms.Button but3;
        internal System.Windows.Forms.Button but4;
        internal System.Windows.Forms.Button but5;
        internal System.Windows.Forms.Button but6;
        internal System.Windows.Forms.Button but7;
        internal System.Windows.Forms.Button but8;
        internal System.Windows.Forms.Button but9;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton VersusComputerButton;
        private System.Windows.Forms.RadioButton TwoPlayersButton;
    }
}

