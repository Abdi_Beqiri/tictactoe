﻿using System;
using System.Drawing;
using System.Windows.Forms;
using TicTacToeGame;

namespace TicTacToeWinForms
{
    public partial class GameForm : Form
    {
        private Button[] _buttonArray = new Button[9];
        private TicTacToe _ticTacToe = new TicTacToe();
        private bool _isTwoPlayers = true;
        private Font _font = new Font("Microsoft Sans Serif", 24F, FontStyle.Regular, GraphicsUnit.Point, 0);

        public GameForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _buttonArray = new Button[9] { but1, but2, but3, but4, but5, but6, but7, but8, but9 };
            for (int i = 0; i < 9; i++)
                _buttonArray[i].Click += new EventHandler(ClickHandler);

            ResetButtons();  // set the defaults
            _ticTacToe.ResetGame();
            _ticTacToe.MovePlayed += new EventHandler<MovePlayedEventArgs>(MovePlayedHandler);
        }

       private void ButtonPlay_Click(object sender, EventArgs e)
        {
            _isTwoPlayers = TwoPlayersButton.Checked;
            ResetButtons();
            _ticTacToe.ResetGame();
            labelTurn.Text = "X's turn";
        }

        private void ResetButtons()
        {
            for (int i = 0; i < 9; i++)
            {
                _buttonArray[i].Tag = i.ToString();
                _buttonArray[i].Text = "";
                _buttonArray[i].ForeColor = Color.Black;
                _buttonArray[i].BackColor = Color.LightGray;
                _buttonArray[i].Font = _font;
                _buttonArray[i].Enabled = true;
            }
        }

        private void ClickHandler(object sender, EventArgs e)
        {
            int buttonPosition = int.Parse(((Button)sender).Tag.ToString());
           
            _ticTacToe.PlayMove(buttonPosition);
            
            if (!_isTwoPlayers)
                _ticTacToe.PlayComputerMove();
        }

        private void MovePlayedHandler(object sender, MovePlayedEventArgs e)
        {
            // Alternate putting the X or O character in the Text property
            labelTurn.Text = _ticTacToe.IsXTurn ? "O's turn" : "X's turn";
            UpdateButton(e.MovePlayed);
            if (e.IsGameOver)
            {
                labelTurn.Text = "Game over";
                ChangeColor(_buttonArray, e.WinnerCombination);
            }
        }

        private void UpdateButton(int i)
        {
            _buttonArray[i].Text = _ticTacToe.IsXTurn ? "X" : "O";
            _buttonArray[i].Enabled = false;
        }

        private void ChangeColor(Button[] buttonArray, int[] winnerCombination)
        {
            var newColor = Color.LightCoral;
            var newFont = new Font(_font, FontStyle.Italic & FontStyle.Bold);
            
            if (winnerCombination != null)
            {
                foreach (var butonId in winnerCombination)
                {
                    _buttonArray[butonId].BackColor = newColor;
                    _buttonArray[butonId].Font = newFont;
                }
            }

            foreach (var button in _buttonArray)
            {
                button.Enabled = false;
            }
        }

        private void TableLayoutPanel1_SizeChanged(object sender, EventArgs e)
        {
            var min = (float)Math.Min(tableLayoutPanel1.Height, tableLayoutPanel1.Width);
            var fontSize = (min / 6);
            _font = new Font("Microsoft Sans Serif", fontSize, FontStyle.Regular);
            foreach (var button in _buttonArray)
            {
                if(button != null && button.Font != null)
                    button.Font = new Font("Microsoft Sans Serif", fontSize, button.Font.Style);
            }
            
        }
    }
}

