﻿using System;

namespace TicTacToeGame
{
    public class Board
    {
        public const int BoardSize = 9;
        const int WinnerCombinationsCount = 3;
        // Winners contains all the array locations of
        // the winning combination -- if they are all 
        // either X or O (and not blank)
        private int[,] _winners = new int[,]
                   {
                        {0,1,2},
                        {3,4,5},
                        {6,7,8},
                        {0,3,6},
                        {1,4,7},
                        {2,5,8},
                        {0,4,8},
                        {2,4,6}
                   };

        private string[] _gameBoard = new string[BoardSize];

        public void ResetBoard()
        {
            _gameBoard = new string[BoardSize];
        }

        public string this[int key]
        {
            get => _gameBoard[key];
            set
            {
                if (key > _gameBoard.Length)
                    throw new IndexOutOfRangeException("Index out of range. One can only set board values between 0 and 8!");

                _gameBoard[key] = value;
            }
        }

        public bool IsGameOver()
        {           
            return GetWinnerCombination() != null || IsDraw();
        }

        public int[] GetWinnerCombination()
        {
            const int WinnersCount = 8;

            // Check win
            for (int i = 0; i < WinnersCount; i++)
            {
                // get the indices of the winners
                int a = _winners[i, 0], b = _winners[i, 1], c = _winners[i, 2];

                // just to make the code readable
                string b1 = _gameBoard[a], b2 = _gameBoard[b], b3 = _gameBoard[c];

                // any of the squares blank
                if (b1 == null || b2 == null || b3 == null)
                    continue;        // try another -- no need to go further

                // are they the same?
                if (b1 == b2 && b2 == b3)
                
                    // if so, they WIN!
                    return new int[WinnerCombinationsCount] { a, b, c };                
            }
            return null;
        }        

        private bool IsDraw()
        {
            // Check draw
            var isDraw = true;
            foreach (var value in _gameBoard)
                isDraw &= value != null;

            return isDraw;
        }
    }
}
